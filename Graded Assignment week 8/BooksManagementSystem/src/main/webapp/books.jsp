<%@page import="java.util.Arrays"%>
<%@page import="com.greatlearning.bean.Book"%>
<%@page import="com.greatlearning.dao.BooksDao"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books</title>
<link rel="stylesheet" href="assets/styles.css" type="text/css">
</head>
<body>
	<div class="nav" id="nav" style="background-color: transparent;">
		<a href="books">Books</a>
		<div class="join-box">
			<p class="join-msg">
				<a href="readLaterbooks">Read Later</a> <a href="likedbooks">Liked</a>
				<%
				String email = (String) session.getAttribute("email");
				if (email == null) {
				%>
				<a href="login">Sign in</a>
				<%
				} else {
				%>
				<a><%=email%></a> <a href="logout">Log Out</a>
				<%
				}
				%>
			</p>
		</div>
	</div>

	<div class='content'>
		<%
		List<Book> books = (List<Book>) session.getAttribute("books");
		for (Book book : books) {
		%>
		<div class='row'>
			<p><%=book.getName()%></p>
			<img class="row_image" alt="<%=book.getName()%>"
				src="assets/images/<%=book.getPoster()%>">
			<p>
				Author :<%=book.getAuthor_name()%></p>
			<form action="favbutton" method="post">
				<input type="hidden" id="thisField" name="bookId"
					value="<%=book.getBookId()%>">

				<button type="submit" class="fav">
					Like</button>
			</form>
			<form action="readlaterbutton" method="post">
				<input type="hidden" id="thisField" name="bookId"
					value="<%=book.getBookId()%>">

				<button type="submit" class="readLater">
					Read Later</button>
			</form>
			


		</div>
		<%
		}
		%>

	</div>
</body>
</html>