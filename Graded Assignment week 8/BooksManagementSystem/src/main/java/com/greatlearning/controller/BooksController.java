package com.greatlearning.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.greatlearning.dao.*;
import com.greatlearning.bean.*;

@Controller
public class BooksController {

	@Autowired
	private BooksDao bookDB;
	

	public BooksController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping("/books")
	public String mainBooks(HttpSession session) {
		try {
			List<Book> books= bookDB.getAllBooks();
			session.setAttribute("books", books);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "books";
	}
	
	@GetMapping("/likedbooks")
	public String liksedBooks(HttpSession session) {
		String email = (String) session.getAttribute("email");
		if(email!=null) {
		List<Book> books = bookDB.getlikedBooks(email);
		session.setAttribute("books", books);
		session.setAttribute("from", "likedbooks");
		return "books2";
		}
		else
		return "login";
		
		
	}
	@GetMapping("/readLaterbooks")
	public String readLaterBooks(HttpSession session) {
		String email = (String) session.getAttribute("email");
		if(email!=null) {
		List<Book> books = bookDB.getReadLaterBooks(email);
		session.setAttribute("books", books);
		session.setAttribute("from", "readLaterbooks");
		return "books2";
		}
		else
		return "login";
	}
	
	@GetMapping("/favbutton")
	public String getLiked() {
		return "books";
	}
	
	@PostMapping("/favbutton")
	public String liked(@ModelAttribute("bookId") int bookId,HttpSession session) {
		System.out.println(bookId);
		String email = (String) session.getAttribute("email");
		if(email!=null) {
		bookDB.addFavBook(bookId, email);
		return "books";
		}
		else
		return "login";
	}
	
	@PostMapping("/removefav")
	public String removefav(@ModelAttribute("bookId") int bookId,HttpSession session) {
		String email = (String) session.getAttribute("email");
		
		try {
			bookDB.removefavbook(bookId, email);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Book> books = bookDB.getlikedBooks(email);
		session.setAttribute("books", books);
		session.setAttribute("from", "likedbooks");
		return "books2";
	}
	
	@GetMapping("/readlaterbutton")
	public String getReadLater() {
		return "books";
	}
	
	@PostMapping("/readlaterbutton")
	public String readlater(@ModelAttribute("bookId") int bookId,HttpSession session) {
		
		System.out.println(bookId);
		String email = (String) session.getAttribute("email");
		if(email!=null) {
		bookDB.addReadlaterBook(bookId, email);
		return "books";
		}
		else
		return "login";
	}
	
	@PostMapping("/removereadlater")
	public String removereadlater(@ModelAttribute("bookId") int bookId,HttpSession session) {
		String email = (String) session.getAttribute("email");
		
		try {
			bookDB.removeReadlaterBook(bookId, email);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Book> books = bookDB.getReadLaterBooks(email);
		session.setAttribute("books", books);
		session.setAttribute("from", "readLaterbooks");
		return "books2";
	}
	
	@GetMapping("/removefav")
	public String getRemoveFav() {
		return "likedbooks";
	}
	@GetMapping("removereadlater")
	public String getRemoveReadlater() {
		return "readLaterbooks";
	}
	

}
