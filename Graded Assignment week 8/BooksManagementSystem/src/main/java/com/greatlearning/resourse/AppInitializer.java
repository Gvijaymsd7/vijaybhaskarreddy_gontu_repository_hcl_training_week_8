package com.greatlearning.resourse;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.greatlearning.resourse.WebAppConfig;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	public AppInitializer() {
		System.out.println("app initalizer loaded");
	}

    @Override
     protected Class<?>[] getRootConfigClasses() {
          return null;
     }

     @Override
     protected Class<?>[] getServletConfigClasses() {
    	 System.out.println("app initializer");
         return new Class<?>[] { WebAppConfig.class };
     }

     @Override  
     protected String[] getServletMappings() {
          return new String[] { "/" };
     }


}



