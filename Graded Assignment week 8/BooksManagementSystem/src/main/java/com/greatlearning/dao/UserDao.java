package com.greatlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.greatlearning.dao.*;
import com.greatlearning.bean.*;
@Repository
public class UserDao {
	@Autowired
	private JdbcTemplate template;

	public boolean insertUser(User user) throws SQLException {
		
		String sql = "insert into user(username, email,phone,password) values(?,?,?,?)";

		template.update(sql, user.getUserName(), user.getEmail(), user.getPhone(), user.getPassword());

		return true;
	}


	public boolean validate(LoginUser loginUser) {

		String email = loginUser.getEmail(); 
		String pwd= loginUser.getPassword();
		String sql = "select email, password, phone, userName from user where email = ?";
		User user;
		try{
			user = this.template.queryForObject(sql, new UserRowMapper(), email);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		if(user.getEmail().equals(email) && user.getPassword().equals(pwd))
			return true;
		return false;
	}

	public int getUserId(String email) throws SQLException {

		String sql = "select userId from user where email ='" + email + "';";
		List<User> list = this.template.query(sql, (ResultSet rs, int arg1)->{
			User user = new User();
			user.setUsedId(rs.getInt(1));
			return user;	
	});
		int userId=0;
		for(User user:list) {
			userId = user.getUsedId();
		}
		return userId;
	}
	class UserRowMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setEmail(rs.getString(1));
		user.setPassword(rs.getString(2));
		user.setPhone(rs.getString(3));
		user.setUserName(rs.getString(4));
		return user;
	}
		
	}
}
